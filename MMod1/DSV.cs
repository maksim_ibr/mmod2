﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;

namespace MMod1
{
    internal delegate double DistributionMethod(Dictionary<string, double> input, int value);

    internal static class Dsv
    {
        internal static List<double> GenerateDsv(this DistributionMethod gen, Dictionary<string, double> input,
            int length, int valuesCount)
        {
            if (length < 10)
            {
                MessageBox.Show("Для адекватного результата >= 10");
                Environment.Exit(1);
            }

            var test = 0.0;
            var segments = new List<Tuple<double, double>>(length + 1);
            for (var i = 0; i < length; i++)
            {
                var offset = gen.Invoke(input, i);
                test += offset;
                segments.Add(segments.Count == 0
                    ? new Tuple<double, double>(0, offset)
                    : new Tuple<double, double>(segments[i - 1].Item2, segments[i - 1].Item2 + offset));

                if (Math.Abs(test - 1) < Conditions.Eps || offset < 0)
                {
                    MessageBox.Show("Ошибка вероятности");
                    Environment.Exit(1);
                }
            }

            var random = new CustomRandom();
            var randomValues = new List<double>(valuesCount + 1);
            for (var i = 0; i < valuesCount; i++)
            {
                randomValues.Add(random.NextDouble());
            }

            var result = new List<double>(length);
            for (var i = 0; i < length; i++)
            {
                result.Add(randomValues.Count(x => x >= segments[i].Item1 && x <= segments[i].Item2) * 100.0 /
                           valuesCount);
            }

            return result;
        }
    }
}
