﻿using System;
using System.Collections.Generic;

namespace MMod1
{
    internal static class Distributions
    {
        private const double Fault = 0.000001;
        internal static int DualDistributionHeight => DualDistribution.Length;
        internal static int DualDistributionWidth(int x) => DualDistribution[x].Length;

        internal static readonly double[][] DualDistribution = {
            new[] { 0.05, 0.05, 0.05, 0.05, 0.15 },
            new[] { 0.02, 0.02, 0.02, 0.02, 0.02 },
            new[] { 0.05, 0.05, 0.05, 0.05, 0.05 },
            new[] { 0.02, 0.02, 0.02, 0.02, 0.02 },
            new[] { 0.15, 0.01, 0.01, 0.01, 0.02 }
        };

        internal static double GetDualDistributionProbability(int x, int y) => DualDistribution[x][y];

        internal static double GetNewProbability(Dictionary<string, double> input, int value)
        {
            return 1 / input["l"];
        }

        internal static double GetGeometricProbability(Dictionary<string, double> input, int value) =>
            Math.Pow(1 - input["p"], value) * input["p"];

        internal static double GetСustomProbability(Dictionary<string, double> input, int value) =>
            Math.Abs(value - input["f"]) < Fault
                ? 0.1
                : Math.Abs(value - input["s"]) < Fault
                    ? 0.8
                    : Math.Abs(value - input["t"]) < Fault
                        ? 0.1
                        : 0.0;

    }
}
