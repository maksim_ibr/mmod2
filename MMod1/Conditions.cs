﻿namespace MMod1
{
    internal static class Conditions
    {
        internal const int From = 0;
        internal const int To = 30;
        internal const int Length = To - From;
        internal const int Count = 10000000;
        internal const double Eps = 0.00000000000000000004;


        internal static readonly Methods SelectedMethod = Methods.Ddsv;
        internal static readonly DistributionCollection SelectedDistributionMethod = DistributionCollection.Normal;
    }
}
