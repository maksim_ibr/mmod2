﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MathNet.Numerics.Distributions;
using System.Windows;

namespace MMod1
{
    internal static class Assessments
    {
        private const double StudentValue10000 = 1.95996422177;
        private const double Y = 0.95;

        internal static double MathematicalExpectation(this ObservableCollection<KeyValuePair<int, double>> samples,
            bool square = false)
        {
            var result = 0.0;
            foreach (var sample in samples)
            {
                result += sample.Value / 100.0 * (square ? Math.Pow(sample.Key, 2) : sample.Key);
            }

            return result;
        }

        internal static Tuple<double, double> DoubleMathematicalExpectation(this double[][] samples,
            bool square = false)
        {
            var mX = 0.0;
            var maxYLength = 0;
            for (var x = 0; x < Distributions.DualDistributionHeight; x++)
            {
                mX += samples[x].Sum() / 100.0 * (square ? Math.Pow(x, 2) : x);
                maxYLength = maxYLength >= Distributions.DualDistributionWidth(x)
                    ? maxYLength
                    : Distributions.DualDistributionWidth(x);
            }

            var pYList = new List<double>(maxYLength + 1);
            for (var i = 0; i < maxYLength; i++)
            {
                pYList.Add(0.0);
            }

            for (var x = 0; x < Distributions.DualDistributionHeight; x++)
            {
                for (var y = 0; y < Distributions.DualDistributionWidth(x); y++)
                {
                    pYList[y] += samples[x][y] / 100.0;
                }
            }

            var mY = pYList.Select((p, i) => p * (square ? Math.Pow(i, 2) : i)).Sum();

            return new Tuple<double, double>(mX, mY);
        }

        internal static double Dispertion(this ObservableCollection<KeyValuePair<int, double>> samples) =>
            MathematicalExpectation(samples, true) - Math.Pow(MathematicalExpectation(samples), 2);

        internal static Tuple<double, double> DoubleDispertion(this double[][] samples)
        {
            var doubleMathematicalExpectationSquare = DoubleMathematicalExpectation(samples, true);
            var doubleMathematicalExpectation = DoubleMathematicalExpectation(samples);

            var dX = doubleMathematicalExpectationSquare.Item1 - Math.Pow(doubleMathematicalExpectation.Item1, 2);
            var dY = doubleMathematicalExpectationSquare.Item2 - Math.Pow(doubleMathematicalExpectation.Item2, 2);

            return new Tuple<double, double>(dX, dY);
        }

        internal static double DottedMathematicalExpectation(this ObservableCollection<KeyValuePair<int, double>> samples,
            int count) => (double)count / (count - 1) * Dispertion(samples);

        internal static Tuple<double, double> DoubleDottedMathematicalExpectation(this double[][] samples, int count)
        {
            var dispertion = DoubleDispertion(samples);

            var ddX = (double)count / (count - 1) * dispertion.Item1;
            var ddY = (double)count / (count - 1) * dispertion.Item2;

            return new Tuple<double, double>(ddX, ddY);
        }

        internal static Tuple<double, double> ConfidenceIntervalForMathematicalExpectation(
            this ObservableCollection<KeyValuePair<int, double>> samples, int count)
        {
            var dottedMathematicalExpectationSqrt = Math.Sqrt(DottedMathematicalExpectation(samples, count));
            var mathematicalExpectation = MathematicalExpectation(samples);
            var value = dottedMathematicalExpectationSqrt * StudentValue10000 / Math.Sqrt(count - 1);
            var left = mathematicalExpectation - value;
            var right = mathematicalExpectation + value;

            return new Tuple<double, double>(left, right);
        }

        internal static Tuple<Tuple<double, double>, Tuple<double, double>>
            DoubleConfidenceIntervalForMathematicalExpectation(this double[][] samples, int count)
        {
            var dottedMathematicalExpectation = DoubleDottedMathematicalExpectation(samples, count);
            var dottedMathematicalExpectationSqrtX = Math.Sqrt(dottedMathematicalExpectation.Item1);
            var dottedMathematicalExpectationSqrtY = Math.Sqrt(dottedMathematicalExpectation.Item2);
            var mathematicalExpectation = DoubleMathematicalExpectation(samples);

            var valueX = dottedMathematicalExpectationSqrtX * StudentValue10000 / Math.Sqrt(count - 1);
            var valueY = dottedMathematicalExpectationSqrtY * StudentValue10000 / Math.Sqrt(count - 1);

            var leftX = mathematicalExpectation.Item1 - valueX;
            var leftY = mathematicalExpectation.Item2 - valueY;
            var rightX = mathematicalExpectation.Item1 + valueX;
            var rightY = mathematicalExpectation.Item2 + valueY;

            return new Tuple<Tuple<double, double>, Tuple<double, double>>(new Tuple<double, double>(leftX, rightX),
                new Tuple<double, double>(leftY, rightY));
        }

        internal static Tuple<double, double> ConfidenceIntervalForDispertion(
            this ObservableCollection<KeyValuePair<int, double>> samples, int count)
        {
            var dottedMathematicalExpectation = DottedMathematicalExpectation(samples, count);
            var left = count * dottedMathematicalExpectation / ChiSquared.InvCDF(count - 1, (1 - Y) / 2);
            var right = count * dottedMathematicalExpectation / ChiSquared.InvCDF(count - 1, (1 + Y) / 2);

            return new Tuple<double, double>(left, right);
        }

        internal static Tuple<Tuple<double, double>, Tuple<double, double>> DoubleConfidenceIntervalForDispertion(
            this double[][] samples, int count)
        {
            var dottedMathematicalExpectation = DoubleDottedMathematicalExpectation(samples, count);
            var leftX = count * dottedMathematicalExpectation.Item1 / ChiSquared.InvCDF(count - 1, (1 - Y) / 2);
            var rightX = count * dottedMathematicalExpectation.Item1 / ChiSquared.InvCDF(count - 1, (1 + Y) / 2);
            var leftY = count * dottedMathematicalExpectation.Item2 / ChiSquared.InvCDF(count - 1, (1 - Y) / 2);
            var rightY = count * dottedMathematicalExpectation.Item2 / ChiSquared.InvCDF(count - 1, (1 + Y) / 2);

            return new Tuple<Tuple<double, double>, Tuple<double, double>>(new Tuple<double, double>(leftX, rightX),
                new Tuple<double, double>(leftY, rightY));
        }

        internal static double DoubleCorrelation(this double[][] samples)
        {
            var mathematicalExpectation = DoubleMathematicalExpectation(samples);

            var covariance = 0.0;
            for (var x = 0; x < Distributions.DualDistributionHeight; x++)
            {
                for (var y = 0; y < Distributions.DualDistributionWidth(x); y++)
                {
                    covariance += x * y * (samples[x][y] / 100.0);
                }
            }

            covariance = covariance - mathematicalExpectation.Item1 * mathematicalExpectation.Item2;

            var dispertion = DoubleDispertion(samples);
            var standardDeviationX = Math.Sqrt(dispertion.Item1);
            var standardDeviationY = Math.Sqrt(dispertion.Item2);

            var result = covariance / standardDeviationX * standardDeviationY;

            return result;
        }

        internal static bool CriterionChiSquared(this ObservableCollection<KeyValuePair<int, double>> samples,
            Dictionary<string, double> input, int count, DistributionCollection distribution)
        {
            var currentP = 0.0;
            var result = 0.0;
            var counter = 0;
            foreach (var sample in samples)
            {
                currentP += sample.Value / 100.0;
                if (counter++ % 2 == 1)
                {
                    continue;
                }

                double theoreticalP;
                switch (distribution)
                {
                    case DistributionCollection.Geometric:
                        theoreticalP = Geometric.CDF(input["p"], sample.Key + 1);
                        break;
                    // ToDo Для расширяемости
                    default:
                        MessageBox.Show("Распределение не задано (CriterionChiSquared)");
                        return false;
                }

                result += count * Math.Pow(currentP - theoreticalP, 2) / theoreticalP;
            }

            return result < ChiSquared.InvCDF(samples.Count - input.Count - 1, Y);
        }

        internal static bool DoubleCriterionChiSquared(this double[][] samples, double[][] theoreticalSamples,
            int count)
        {
            var counter = 0;
            var result = 0.0;
            var currentP = 0.0;
            var theoreticalP = 0.0;
            for (var x = 0; x < samples.Length; x++)
            {
                for (var y = 0; y < samples[x].Length; y++)
                {
                    counter++;
                    currentP += samples[x][y] / 100.0;
                    theoreticalP += theoreticalSamples[x][y];
                    if (counter % 3 == 0)
                    {
                        result += count * Math.Pow(currentP - theoreticalP, 2) / theoreticalP;
                    }
                }
            }

            return result < ChiSquared.InvCDF(counter - 1, Y);
        }

        internal static double FTest(this double ed, double td) => ed / td;

        internal static Tuple<double, double> ZTest(this double[][] e, double[][] t, int count)
        {
            var d1 = DoubleDispertion(e);
            var d2 = DoubleDispertion(t);

            var x1 = 0.0;
            var x2 = 0.0;
            var y1 = 0.0;
            var y2 = 0.0;

            for (var x = 0; x < e.Length; x++)
            {
                var probXe = 0.0;
                var probYe = 0.0;
                var probXt = 0.0;
                var probYt = 0.0;
                
                for (var y = 0; y < e[x].Length; y++)
                {
                    probXe += e[x][y];
                    probYe += e[y][x];
                    probXt += t[x][y];
                    probYt += t[y][x];
                }

                x1 += probXe * x / 100;
                y1 += probYe * x / 100;
                x2 += probXt * x / 100;
                y2 += probYt * x / 100;
            }

            var zx = (x1 - x2) / Math.Sqrt(d1.Item1 / count + d2.Item1 / count);
            var zy = (y1 - y2) / Math.Sqrt(d1.Item2 / count + d2.Item2 / count);

            return new Tuple<double, double>(zx, zy);
        }
    }
}
