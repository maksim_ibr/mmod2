﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace MMod1
{
    /// <inheritdoc>
    ///     <cref></cref>
    /// </inheritdoc>
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    public partial class MainWindow
    {
       
        private readonly ObservableCollection<KeyValuePair<int, double>> _values;

        public MainWindow()
        {
            InitializeComponent();

            _values = new ObservableCollection<KeyValuePair<int, double>>();

            switch (Conditions.SelectedMethod)
            {
                case Methods.Dsv:
                    HideDdsvControls();
                    GenerateDsv();
                    break;
                case Methods.Ddsv:
                    GenerateDdsv();
                    break;
                default:
                    MessageBox.Show("Выберите тип ДСВ.");
                    Environment.Exit(1);
                    break;
            }

            ShowColumnCharts();
        }

        private void GenerateDsv()
        {
            DistributionMethod method = null;
            var input = new Dictionary<string, double>();

            switch (Conditions.SelectedDistributionMethod)
            {
                case DistributionCollection.Geometric:
                    method = Distributions.GetGeometricProbability;
                    input.Add("p", 0.6);
                    break;
                case DistributionCollection.Сustom:
                    method = Distributions.GetСustomProbability;
                    input.Add("f", 1);
                    input.Add("s", 7);
                    input.Add("t", 9);
                    break;
                case DistributionCollection.Normal:
                    input.Add("l", 30.0);
                    method = Distributions.GetNewProbability;
                    break;
                default:
                    MessageBox.Show("Выберите метод распределения.");
                    Environment.Exit(1);
                    break;
            }

            var counter = 0;
            var result = method.GenerateDsv(input, Conditions.Length, Conditions.Count);
            for (var i = Conditions.From; i < Conditions.To; i++)
            {
                var column = new KeyValuePair<int, double>(i, result[counter++]);
                _values.Add(column);
            }

            DMx.Text = $"{_values.MathematicalExpectation():0.0000}";
            DDx.Text = $"{_values.Dispertion():0.0000}";
            var confidenceIntervalMn = _values.ConfidenceIntervalForMathematicalExpectation(Conditions.Count);
            var confidenceIntervalDn = _values.ConfidenceIntervalForDispertion(Conditions.Count);
            DIMx.Text = $"{confidenceIntervalMn.Item1:0.0000}<=Mx<{confidenceIntervalMn.Item2:0.0000}";
            DIDx.Text = $"{confidenceIntervalDn.Item2:0.0000}<=Dx<{confidenceIntervalDn.Item1:0.0000}";
            if (Conditions.SelectedDistributionMethod != DistributionCollection.Сustom)
            {
                Pirson.Text =
                    _values.CriterionChiSquared(input, Conditions.Count, Conditions.SelectedDistributionMethod)
                        ? "Подтвердил"
                        : "Опроверг";
            }
        }

        private void GenerateDdsv()
        {
            var result = Distributions.DualDistribution.GenerateDdsv(Conditions.Count);

            InitEmpiricalMatrix(result);

            var vectors = new Tuple<List<double>, List<double>>(new List<double>(), new List<double>());
            for (var x = 0; x < Distributions.DualDistributionHeight; x++)
            {
                vectors.Item1.Add(result[x].Sum());
                for (var y = 0; y < Distributions.DualDistributionWidth(x); y++)
                {
                    if (vectors.Item2.Count < y + 1)
                    {
                        vectors.Item2.Add(0.0);
                    }

                    vectors.Item2[y] += result[x][y];
                }
            }

            var counter = 0;
            for (var i = 0; i < vectors.Item1.Count; i++)
            {
                var x = new KeyValuePair<int, double>(counter++, vectors.Item1[i]);
                var y = new KeyValuePair<int, double>(counter++, vectors.Item2[i]);
                _values.Add(x);
                _values.Add(y);
                _values.Add(new KeyValuePair<int, double>(counter++, 0.0));

            }

            var mathematicalExpectation = result.DoubleMathematicalExpectation();
            var dispertion = result.DoubleDispertion();
            var confidenceIntervalForMathematicalExpectation =
                result.DoubleConfidenceIntervalForMathematicalExpectation(Conditions.Count);
            var confidenceIntervalForDispertion = result.DoubleConfidenceIntervalForDispertion(Conditions.Count);

            DMxLabel.Text += "\r\nM[Y]";
            DDxLabel.Text += "\r\nD[Y]";
            CC.Text = $"{result.DoubleCorrelation():0.0000}";
            DMx.Text = $"{mathematicalExpectation.Item1:0.0000}\r\n{mathematicalExpectation.Item2:0.0000}";
            DDx.Text = $"{dispertion.Item1:0.0000}\r\n{dispertion.Item2:0.0000}";
            DIMx.Text = $"{confidenceIntervalForMathematicalExpectation.Item1.Item1:0.0000}" +
                        $"<=Mx<{confidenceIntervalForMathematicalExpectation.Item1.Item2:0.0000}\r\n" +
                        $"{confidenceIntervalForMathematicalExpectation.Item2.Item1:0.0000}" +
                        $"<=My<{confidenceIntervalForMathematicalExpectation.Item2.Item2:0.0000}";
            DIDx.Text = $"{confidenceIntervalForDispertion.Item1.Item1:0.0000}" +
                        $"<=Mx<{confidenceIntervalForDispertion.Item1.Item2:0.0000}\r\n" +
                        $"{confidenceIntervalForDispertion.Item2.Item1:0.0000}" +
                        $"<=My<{confidenceIntervalForDispertion.Item2.Item2:0.0000}";
            Pirson.Text = result.DoubleCriterionChiSquared(Distributions.DualDistribution, Conditions.Count)
                ? "Подтвердил"
                : "Опроверг";

            var tMatrix = new double[Distributions.DualDistributionHeight][];
            Distributions.DualDistribution.CopyTo(tMatrix, 0);
            for (var x = 0; x < tMatrix.Length; x++)
            {
                for (var y = 0; y < tMatrix[x].Length; y++)
                {
                    tMatrix[x][y] *= 100;
                }
            }

            var tDispertion = Distributions.DualDistribution.DoubleMathematicalExpectation();
            dispertion.Item1.FTest(tDispertion.Item1);
            dispertion.Item2.FTest(tDispertion.Item2);
            result.ZTest(Distributions.DualDistribution, Conditions.Count);
        }

        private void ShowColumnCharts() => lineChart.DataContext = _values;


        private void InitEmpiricalMatrix(IReadOnlyList<double[]> matrix)
        {
            _00.Text = $"{matrix[0][0]:0.00}";
            _01.Text = $"{matrix[0][1]:0.00}";
            _02.Text = $"{matrix[0][2]:0.00}";
            _03.Text = $"{matrix[0][3]:0.00}";
            _04.Text = $"{matrix[0][4]:0.00}";
            _10.Text = $"{matrix[1][0]:0.00}";
            _11.Text = $"{matrix[1][1]:0.00}";
            _12.Text = $"{matrix[1][2]:0.00}";
            _13.Text = $"{matrix[1][3]:0.00}";
            _14.Text = $"{matrix[1][4]:0.00}";
            _20.Text = $"{matrix[2][0]:0.00}";
            _21.Text = $"{matrix[2][1]:0.00}";
            _22.Text = $"{matrix[2][2]:0.00}";
            _23.Text = $"{matrix[2][3]:0.00}";
            _24.Text = $"{matrix[2][4]:0.00}";
            _30.Text = $"{matrix[3][0]:0.00}";
            _31.Text = $"{matrix[3][1]:0.00}";
            _32.Text = $"{matrix[3][2]:0.00}";
            _33.Text = $"{matrix[3][3]:0.00}";
            _34.Text = $"{matrix[3][4]:0.00}";
            _40.Text = $"{matrix[4][0]:0.00}";
            _41.Text = $"{matrix[4][1]:0.00}";
            _42.Text = $"{matrix[4][2]:0.00}";
            _43.Text = $"{matrix[4][3]:0.00}";
            _44.Text = $"{matrix[4][4]:0.00}";
        }

        private void HideDdsvControls()
        {
            MatrixLabel.Visibility = Visibility.Hidden;
            CCLabel.Visibility = Visibility.Hidden;
            CC.Visibility = Visibility.Hidden;
            _00.Visibility = Visibility.Hidden;
            _01.Visibility = Visibility.Hidden;
            _02.Visibility = Visibility.Hidden;
            _03.Visibility = Visibility.Hidden;
            _04.Visibility = Visibility.Hidden;
            _10.Visibility = Visibility.Hidden;
            _11.Visibility = Visibility.Hidden;
            _12.Visibility = Visibility.Hidden;
            _13.Visibility = Visibility.Hidden;
            _14.Visibility = Visibility.Hidden;
            _20.Visibility = Visibility.Hidden;
            _21.Visibility = Visibility.Hidden;
            _22.Visibility = Visibility.Hidden;
            _23.Visibility = Visibility.Hidden;
            _24.Visibility = Visibility.Hidden;
            _30.Visibility = Visibility.Hidden;
            _31.Visibility = Visibility.Hidden;
            _32.Visibility = Visibility.Hidden;
            _33.Visibility = Visibility.Hidden;
            _34.Visibility = Visibility.Hidden;
            _40.Visibility = Visibility.Hidden;
            _41.Visibility = Visibility.Hidden;
            _42.Visibility = Visibility.Hidden;
            _43.Visibility = Visibility.Hidden;
            _44.Visibility = Visibility.Hidden;
        }
    }

    internal class ConvertToBrush : IValueConverter
    {
        private int _counter;

        public ConvertToBrush()
        {
            _counter = 1;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var color = Conditions.SelectedMethod == Methods.Dsv ? Colors.Blue :
                _counter++ == 2 ? Colors.DarkRed : Colors.Blue;
            _counter = _counter > 2 ? 0 : _counter;
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture) => null;
    }

    internal enum DistributionCollection
    {
        Geometric = 0,
        Сustom = 1,
        Normal
    }

    internal enum Methods
    {
        Dsv = 0,
        Ddsv = 1
    }
}
