﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace MMod1
{
    internal static class Ddsv
    {
        internal static double[][] GenerateDdsv(this double[][] dualDistribution, int valuesCount)
        {
            var test = 0.0;
            var counter = 0;
            var segments = new List<Tuple<double, double>>();
            for (var x = 0; x < Distributions.DualDistributionHeight; x++)
            {
                for (var y = 0; y < Distributions.DualDistributionWidth(x); y++)
                {
                    var offset = Distributions.GetDualDistributionProbability(x, y);
                    segments.Add(segments.Count == 0
                        ? new Tuple<double, double>(0, offset)
                        : new Tuple<double, double>(segments[counter - 1].Item2, segments[counter - 1].Item2 + offset));
                    counter++;
                    test += offset;
                    if (Math.Abs(test - 1) < Conditions.Eps|| offset < 0)
                    {
                        MessageBox.Show("Ошибка вероятности");
                        Environment.Exit(1);
                    }
                }
            }

            var random = new CustomRandom();
            var randomValues = new List<double>(valuesCount + 1);
            for (var i = 0; i < valuesCount; i++)
            {
                randomValues.Add(random.NextDouble());
            }

            var result = new double[Distributions.DualDistributionHeight][];
            for (var i = 0; i < Distributions.DualDistributionHeight; i++)
            {
                result[i] = new double[Distributions.DualDistributionWidth(i)];
            }

            for (var i = 0; i < segments.Count; i++)
            {
                var x = i / Distributions.DualDistributionHeight;
                var y = i % Distributions.DualDistributionWidth(x);
                result[x][y] = randomValues.Count(v => v >= segments[i].Item1 && v <= segments[i].Item2) * 100.0 / valuesCount;
            }

            return result;
        }
    }
}
