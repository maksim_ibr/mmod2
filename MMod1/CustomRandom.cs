﻿using System;

namespace MMod1
{
    internal class CustomRandom
    {
        private long _next = 7;
        private const long Mask = int.MaxValue;
        private const long Multiplier = 109;

        internal long Next(int fromValue, int toValue)
        {
            while (true)
            {
                var result = (long)Math.Round(fromValue + (toValue - fromValue) * NextDouble());
                if (result >= toValue || result <= fromValue)
                {
                    continue;
                }

                return result;
            }
        }

        internal double NextDouble()
        {
            _next = Multiplier * _next % Mask;
            return (double)_next / Mask;
        }
    }
}
