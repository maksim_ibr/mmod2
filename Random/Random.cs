﻿using System;

namespace Random
{
    internal class Random
    {
        public int IterationsCount { get; set; } = 100000;

        private static long ToDecimal(string binaryNamber) => Convert.ToInt64(binaryNamber, 2);

        private static string ToBinary(long decimalNumber) => Convert.ToString(decimalNumber, 2);


        public long? Next(int fromValue, int toValue) /* Метод перемешивания */
        {
            if (fromValue >= toValue)
            {
                return null;
            }

            var tiks = DateTime.Now.Ticks.ToString();
            var startValue = ToBinary(int.Parse(tiks.Substring(tiks.Length - 3)));

            var startValueLength = startValue.Length;
            var oneQuarter = startValueLength / 4;

            for (var i = 0; i < IterationsCount; ++i)
            {
                

                var leftShiftBinary = startValue.Substring(oneQuarter) + startValue.Substring(0, oneQuarter);
                var rightShiftBinary = startValue.Substring(startValueLength - oneQuarter) +
                                       startValue.Substring(0, startValueLength - oneQuarter);

                startValue = ToBinary(ToDecimal(leftShiftBinary) + ToDecimal(rightShiftBinary));
                startValue = startValue.Length > startValueLength
                    ? startValue.Substring(0, startValueLength)
                    : startValue;
            }

            var result = (long)(fromValue + (toValue - fromValue) * decimal.Parse($"0.{ToDecimal(startValue)}"));

            return result;
        }
    }
}
